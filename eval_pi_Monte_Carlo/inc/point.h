/* header point.h : defines the structure point */


#ifndef _POINT_H
#define _POINT_H

typedef struct Point
{
    long double x;
    long double y;
} POINT;

#endif /* _POINT_H */