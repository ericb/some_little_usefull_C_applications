#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>

#include <sys/time.h>  /* gettimeofday() */
#include <unistd.h>    /* CLOCK_REALTIME */
#include <time.h>      /*    timespec    */
#include <math.h>
#include <stdbool.h>
#include <ctype.h>

#include "point.h"  /* struct POINT */ 

const long int  nbr_points = 1000000;

/* C structure containing a given time in microsecond, and more */
struct timeval tv;
struct timespec tspec;

/* declaration forward */
bool b_dans_le_cercle(POINT *);

bool b_dans_le_cercle(POINT * p_point)
{
    bool toReturn = false;

    if ((sqrtl(p_point->x*p_point->x + p_point->y*p_point->y)) <= 1.0000)
        toReturn = true;

    return toReturn;
}


int main (void)
{
  POINT p;
  p.x = 0.0f;
  p.y = 0.0f;

  POINT * p_point = &p;

  long int points_dedans = 0;
  long int utime1 = 0;


  for (long int i=0 ; i < nbr_points ; i++)
  {

      clock_gettime(CLOCK_REALTIME, &tspec);
#ifdef DEBUG
      fprintf(stdout, "Time with tspec : %ld \n", tspec.tv_nsec);
#endif
      gettimeofday(&tv, NULL);
      utime1 = tv.tv_usec;
      srand(utime1);

#ifdef DEBUG
      fprintf(stdout, "Date du tirage (en usec): %ld \n", tv.tv_usec);
#endif
      long int x = rand()%999999 +1;
      long int y = rand()%999999 +1;

      p.x = (long double)x /(long double)1000000.0f;
      p.y = (long double)y /(long double)1000000.0f;

#ifdef DEBUG
      fprintf(stdout,"Tirage : \n");
      fprintf (stdout, "p.x = %Lf ; p.y = %Lf \n", p.x, p.y);
#endif
      if (b_dans_le_cercle(p_point))
          points_dedans++;
  }


  long double estimated_pi = (long double)4.0 * /*(long double)*/points_dedans / nbr_points;

  fprintf(stdout, "Approximation de pi = %Lf \n", estimated_pi );

  return EXIT_SUCCESS;
}

