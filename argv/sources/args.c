#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define EXTENSION_SIZE_MAX 6

static char extension[EXTENSION_SIZE_MAX] = "";

static void find_extension(char * temp, char ** argv)
{
    // remove the outfile name, including the dot, and save the result
    temp = strtok(temp,".");
    fprintf(stdout,"1er contenu de temp : %s \n", temp);

    temp = strtok(NULL,"");

    fprintf(stdout, "2nd contenu de temp après strtok(NULL,\"\") %s\n", temp);

    if (temp == NULL)
    {
        fprintf(stderr,"saisie incorrecte\n");
        fprintf(stderr,"Usage : %s filename.ext\n", argv[0]);
    }
    else
    {
        strncpy(extension, temp, EXTENSION_SIZE_MAX -1);
        fprintf(stderr,"extension de type : %s \n", extension);
    }
}


int main(int argc, char * argv[])
{
    int i = 0;

    while (argv[i] != NULL)
    {
        fprintf(stdout, "Argument %d = %s\n", i, argv[i]);
        i++;
    }

    if (argv[1] != NULL)
    {
        char * temp = argv[1];

        find_extension(temp, argv);
    }

    return EXIT_SUCCESS;
}
