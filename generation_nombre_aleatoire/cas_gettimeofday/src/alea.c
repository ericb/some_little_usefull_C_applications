/* alea.c  licence GPL v2  Eric Bachard 2025/01/07 */

#include <stdlib.h>
#include <stdio.h>

#include "initialize_random.h"

#include <sys/time.h>

struct timeval tv;


void initialize_rand2(unsigned int usec_time)
{
    srand(usec_time);
}

int main(void)
{
    gettimeofday(&tv,NULL);
    initialize_rand2((unsigned int)tv.tv_usec);
//#define FORMATAGE
    // affiche un nombre compris entre 0 et 99
#ifndef FORMATAGE
    fprintf(stdout, "Tirage d'un nombre entre 0 et 99 : ");
    fprintf(stdout, "Tirage effectué : %3d\n", rand() %100);
#else
    fprintf(stdout, "%3d  ", rand() %100);
#endif

    return EXIT_SUCCESS;
}
