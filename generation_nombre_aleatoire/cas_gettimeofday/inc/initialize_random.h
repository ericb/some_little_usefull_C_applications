/* initialize_random.h GPL v2, Eric Bachard 2025/01/07 */

#ifndef __INITIALIZE_RANDOM2_H
#define __INITIALIZE_RANDOM2_H
#include <time.h>

void initialize_rand2(unsigned int);


#endif /* __INITIALIZE_RANDOM2_H */
