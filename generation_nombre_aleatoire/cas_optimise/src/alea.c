/* alea.c  licence GPL v2  Eric Bachard 2025/01/07 */

#include <stdlib.h>
#include <stdio.h>

#include "tirage_rand.h"

int main(void)
{
    for (int indice=0 ; indice < 20 ; indice ++)
    {
         fprintf(stdout, "%3d ", tirage_random_number(1, 49));
    }

     fprintf(stdout, "\n");

    return EXIT_SUCCESS;
}
