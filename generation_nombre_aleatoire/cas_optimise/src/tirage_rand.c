/* fichier tirage_rand.c */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "tirage_rand.h"

struct timeval tv;

void initialize_rand(unsigned int usec_time)
{
    srand(usec_time);
}

int tirage_random_number(unsigned int min, unsigned int max)
{
    gettimeofday(&tv, NULL);
    initialize_rand((unsigned int)tv.tv_usec);

    return ((rand() % (max - min + 1)) + min);
}
