/* alea.c  licence GPL v2  Eric Bachard 2025/01/07 */

#include <stdlib.h>
#include <stdio.h>

#include "initialize_random.h"

void initialize_rand()
{
    srand(time(NULL));
}

int main(void)
{
    initialize_rand();
    fprintf(stdout, "Tirage effectué : %d\n", rand() %100);

    return EXIT_SUCCESS;
}
