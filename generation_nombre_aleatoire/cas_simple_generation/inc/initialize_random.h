/* initialize_random.h GPL v2, Eric Bachard 2025/01/07 */

#ifndef __INITIALIZE_RANDOM_H
#define __INITIALIZE_RANDOM_H
#include <time.h>

void initialize_rand();


#endif /* __INITIALIZE_RANDOM_H */
