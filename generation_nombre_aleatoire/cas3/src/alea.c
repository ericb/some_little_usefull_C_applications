/* alea.c  licence GPL v2  Eric Bachard 2025/01/07 */

#include <stdlib.h>
#include <stdio.h>

#include "initialize_random.h"

#include <sys/time.h>

struct timeval tv;

void initialize_rand2(unsigned int usec_time)
{
    srand(usec_time);
}

int main(void)
{
    for (int indice=0 ; indice < 20 ; indice ++)
    {
        int T = 0;

        do
        {
            gettimeofday(&tv,NULL);
            initialize_rand2((unsigned int)tv.tv_usec);
            T = rand() % 47;

         }  while (     ((T < 11) || (T > 16))
                     && ((T < 21) || (T > 26))
                     && ((T < 31) || (T > 36))
                     &&  (T < 41)
                   );

         fprintf(stdout, "%3d ", T);
    }

    fprintf(stdout, "\n");

    return EXIT_SUCCESS;
}
