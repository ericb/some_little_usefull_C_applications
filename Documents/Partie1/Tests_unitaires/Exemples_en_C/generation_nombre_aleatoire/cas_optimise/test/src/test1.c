#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "tirage_rand.h"

int main(void)
{
    int min = 1;
    int max = 100;
    int tirage = -1;

    assert(max >= min);

    for (int indice=0 ; indice < 1000000 ; indice ++)
    {
         tirage = tirage_random_number(min, max);
         assert((tirage >= min) && (tirage <= max));
    }

    fprintf(stdout, "Test successfull (1 000 000 values)\n");

    return EXIT_SUCCESS;

}