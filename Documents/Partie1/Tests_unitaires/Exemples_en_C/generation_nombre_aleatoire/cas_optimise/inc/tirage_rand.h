/* header tirage_rand.h */

#ifndef __TIRAGE_RAND_H_
#define __TIRAGE_RAND_H_

void initialize_rand(unsigned int);

int tirage_random_number(unsigned int, unsigned int);


#endif /* __TIRAGE_RAND_H_ */
