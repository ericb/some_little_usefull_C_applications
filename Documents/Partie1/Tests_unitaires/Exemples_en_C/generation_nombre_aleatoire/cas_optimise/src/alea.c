/* alea.c  licence GPL v2  Eric Bachard 2025/01/07 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "tirage_rand.h"

int main(void)
{
    int min = 1;
    int max = 49;
    int tirage = -1;

//    assert(max >= min);

    for (int indice=0 ; indice < 100000 ; indice ++)
    {
         tirage = tirage_random_number(min, max);
//         assert((tirage >= min) && (tirage <= max));
         fprintf(stdout, "%3d ", tirage);
//         fprintf(stdout, "%3d ", tirage_random_number(min, max));
    }

     fprintf(stdout, "\n");

    return EXIT_SUCCESS;
}
