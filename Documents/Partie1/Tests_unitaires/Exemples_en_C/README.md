But : proposer quelques exemples pour illustrer les objectifs principaux, à savoir

- vérifier la qualité du code ;
- ne pas introduire de régression ;
- documenter en même temps que le code est écrit ;
- écrire en même temps des tests pertinents qui qualifient le code écrit ;
- automatiser les tests ;
- permettre la réutilisabilité du code.


- [] Exemple 1 : génération d'un nombre aléatoire. Granularité de la génération : 1µs
- [] (complete me)

Eric Bachard   2025/01/10


