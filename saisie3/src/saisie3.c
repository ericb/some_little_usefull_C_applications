/* saisie3.c  Bachard Eric  2024_11_07 */

#include <stdio.h>
#include <stdlib.h>

#include "saisie.h"

int main(void)
{
    fprintf(stdout, "Saisir un nombre à virgule court compris entre -100 et 100 :  " );

    float valeur_A = saisie_nombre_reel(-100, 100);

    fprintf(stdout, "Saisir un nombre à virgule court compris entre -100 et 100 :  " );

    float valeur_B = saisie_nombre_reel(-100, 100);


    fprintf(stdout, "La différence vaut : %3.3f \n", valeur_A - valeur_B);

    return EXIT_SUCCESS;
}