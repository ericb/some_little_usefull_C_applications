/* pointeurs1.c  essais de swap sur le tas Eric Bachard 2024/10/25 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint-gcc.h>

int swap_valeurs(uint8_t *, uint8_t *);


int swap_valeurs(uint8_t * pvaleur1, uint8_t * pvaleur2)
{
    uint8_t temp = *pvaleur2;
    fprintf(stdout, "Avant : v1 = %d   v2 = %d  \n", *pvaleur1, *pvaleur2);
    *pvaleur2 = *pvaleur1;
    *pvaleur1 = temp;

    fprintf(stdout, "Après : v1 = %d   v2 = %d  \n", *pvaleur1, *pvaleur2);
    return EXIT_SUCCESS;
}


int main(void)
{
    uint8_t valeur_a = 65;
    uint8_t * pvaleur_a = &valeur_a; /* pvaleur_a contient l'adresse de valeur_a */

    uint8_t valeur_b = 66;
    uint8_t * pvaleur_b = &valeur_b; /* pvaleur_b contient l'adresse de valeur_b */


    fprintf(stdout, "Avant : valeur_a = %d   valeur_b = %d  \n", valeur_a, valeur_b);

    short int anErr = swap_valeurs(pvaleur_a, pvaleur_b);

    if (anErr != 0)
        fprintf(stderr, "Pb pendant le swap des valeurs");

    fprintf(stdout, "Après : valeur_a = %d   valeur_b = %d  \n", valeur_a, valeur_b);

    return EXIT_SUCCESS;
}

