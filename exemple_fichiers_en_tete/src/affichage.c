/* affichage.c */

#include <stdio.h>
#include "affichage.h"
#include "somme.h"

void affichage(float val1, float val2)
{
    float resultat = somme(val1, val2);
    fprintf(stdout, "résultat =  %f, valeurs additionnées : %f et %f\n", resultat, val1, val2);
}

