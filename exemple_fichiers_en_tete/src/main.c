#include <stdio.h>
#include <stdlib.h>

#include "somme.h"
#include "efface_ecran.h"
#include "affichage.h"

int main(void)
{
    efface_ecran();
    affichage(1.23, 12.56);

    return EXIT_SUCCESS;
}


