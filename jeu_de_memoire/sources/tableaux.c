/* tableaux.c  initialisation et remplissage des tableaux de nombres */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "tableaux.h"
#include "random.h"

/* ---------initialisation------------ */

short initialization_tableau_de_depart(int *tableau_initial)
{
    int i;
    for (i = 0; i < ARRAY_SIZE; i++)
    {
        tableau_initial[i] = 0;
    }
    return EXIT_SUCCESS;
}
/* --------------------------------- */

/* ----------tirage_tableau_initial----------- */

short tirage_tableau_initial(int *tableau_initial)
{
    int indice;
    int alea = 0;
    bool bSortie = true;
    initialize_random_stack();

    for ( indice = 0; indice < ARRAY_SIZE ; indice++) 
    {
        do {
            bSortie = true;

            do {
                /* generate a value between VAL_MIN and VAL_MAX */
                alea = rand()% VAL_MAX + 1;
            } while (alea < VAL_MIN);

            /* we must be sure the value does not exist already */
            int valeur;
            for (valeur = 0; valeur < indice ; valeur++)
            {
                if ( tableau_initial[valeur] == alea )
                    bSortie = false;
            }
        }
        while (false == bSortie);

        /* fill the array */
        tableau_initial[indice] = alea;
    }

    return EXIT_SUCCESS;
}

/* --------------------------------- */

/* ----------generer_numero_element_tableau----------- */

short generer_numero_element_tableau(int *tableau_initial)
{
    initialize_random_stack();
    short alea2;

    do {
        /* generate a value between 0 and 9 */
        alea2 = rand() % ARRAY_SIZE;
#ifdef DEBUG
        fprintf(stdout, "ARRAY_SIZE = %d,  alea2 = %d, tableau_initial[%d] =%d \n",ARRAY_SIZE, alea2, alea2, tableau_initial[alea2]);
#endif
    } while(0 == tableau_initial[alea2]);

#ifdef DEBUG
    fprintf(stdout, "tirage réalisé, tableau_initial[%d]= %d \n",alea2, tableau_initial[alea2]);
#endif

    return alea2;
}

/* --------------------------------- */

/* ----------affichage_tableau_initial----------- */

void affichage_tableau_initial(int tableau_initial[])
{
    int i;
    for( i = 0 ; i < ARRAY_SIZE; i++)
    fprintf(stdout,"Tirage %2d : %d   \n",i + 1, tableau_initial[i]);
    fprintf(stdout,"\n");
}

/* --------------------------------- */

/* ----------effacer_element_tableau----------- */

void effacer_element_tableau(int rang, int tableau_initial[])
{
    /* to avoid asking twice the same array element, every checked value is replaced by a zero */
    tableau_initial[rang] = 0;
}

/* --------------------------------- */



