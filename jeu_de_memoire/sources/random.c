/* random.c  cointains initialize random numbers generator + random numbers selection */

#define _GNU_SOURCE

#ifdef DEBUG
#include <stdio.h>
#endif
#include <sys/time.h>  /* gettimeofday() */
#include <unistd.h>    /* CLOCK_REALTIME */
#include <stdlib.h>    /* srand()        */
#include <time.h>      /*    timespec    */
#include "random.h"

/* C structure containing a given time in microsecond, and more */
struct timeval tv;
struct timespec tspec;

void initialize_random_stack(void)
{
    long int utime1 = 0;
    clock_gettime(CLOCK_REALTIME, &tspec);
#ifdef DEBUG
    fprintf(stdout, "Time with tspec : %ld \n", tspec.tv_nsec);
#endif
    gettimeofday(&tv, NULL);
    utime1 = tv.tv_usec;
    srand(utime1);
#ifdef DEBUG
    fprintf(stdout, "%s done ...  \n", __func__);
#endif
}
