/* moteur_jeu.c */

#include <stdio.h>
#include <stdlib.h>

#include "random.h"
#include "saisie.h"
#include "efface_ecran.h"
#include "moteur_jeu.h"
#include "tableaux.h"
#include "constants.h"


/* ----------afficher_question----------- */

short int afficher_question(int value)
{
    const char *suffix;

    switch (value)
    {
        /* the first array element has index 0*/
        case (0):
            suffix = PREMIER;
          break;

        default:
            suffix = ENIEME;
          break;
    }
    fprintf( stdout, "Donner le %d%s element du tableau \n\n",value + 1, suffix);
    return EXIT_SUCCESS;
}

/* --------------------------------- */

/* ---------lire_reponse------------ */

short lire_reponse(void)
{
    //short number;
    short int number = -1;

    while ((number < 0) || (number > VAL_MAX))
    {
        fprintf(stdout, "Entrer un nombre entier court compris entre 0 et %d et appuyer sur entree \n", VAL_MAX);
        number = saisie_nombre_entier_court(0, VAL_MAX);
    }

    efface_ecran();
    return number;
}

/* --------------------------------- */

/* ----------affichage resultat_question----------- */

short affichage_resultat_question(int * tableau_initial, int rang, int number, int *p_nombre_de_points)
{

#ifdef DEBUG
    fprintf(stdout, "vrai = %d ",tableau_initial[rang]);
    fprintf(stdout,"entre = %d \n",number); 
#endif

    if( number == tableau_initial[rang] )
    {
        fprintf(stdout, "Réponse juste !\n\n");
        *p_nombre_de_points += 2;

#ifdef DEBUG
        fprintf(stdout, "Nombre de points local %d \n", *p_nombre_de_points);
#endif

    }
    else
        fprintf(stdout,"C'est faux \n");

    fprintf(stdout," appuyer sur Entree pour continuer");
    getchar();

    efface_ecran();

    return EXIT_SUCCESS;
}
/* --------------------------------- */


/* ----------affichage_resultat_final----------- */


void affichage_resultat_final(int points)
{
    fprintf(stdout, "Nombre de points final =  %d \n", points); 

    if (points < 6)
        fprintf(stdout, "\nCerveau mal entraîné détecté. Il faut t'entraîner davantage !\n"); 
    else if ((points > 4) && (points < 12))
        fprintf(stdout, "\nPeut mieux faire ...  \n"); 
    else if ((points > 10) && (points < 16))
        fprintf(stdout, "\nC'est bien, recommence ...\n"); 
    else if ((points > 14) && (points < 20))
        fprintf(stdout, "\nC'est très bien, recommence ...\n"); 
    else
        fprintf(stdout, "\nBravo, vous avez tout juste !! \n"); 
}

/* --------------------------------- */

/* -------- questionnement --------- */


short int questionnement(int * p_nombre_de_points,  int * tableau_initial)
{

  for(int k = 0; k < ARRAY_SIZE; k++)
  {
    short rang = generer_numero_element_tableau(tableau_initial);
    afficher_question(rang);

    short reponse = lire_reponse();
    affichage_resultat_question(tableau_initial, rang, reponse, p_nombre_de_points);
    effacer_element_tableau(rang,tableau_initial);
  }
  return EXIT_SUCCESS;
}

/* --------------------------------- */




