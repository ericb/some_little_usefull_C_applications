/*                                                   *
 * Auteur : Eric Bachard  version originale : 2004   *
 * Le code est sous Licence GPL v2               *
 * voir : http://www.gnu.org/licenses/gpl-2.0.html   */

#include <stdio.h> 
#include <time.h> 
#include <stdlib.h> 
#include <stdbool.h>

#include "constants.h"
#include "main.h"
#include "efface_ecran.h"
#include "moteur_jeu.h"
#include "tableaux.h"

// local values
static int   nombre_de_points   = 0;
static int * p_nombre_de_points = 0;

static int tableau_initial[ARRAY_SIZE];

void etape1(void)
{
    initialization_tableau_de_depart(tableau_initial);
    tirage_tableau_initial(tableau_initial);

    affichage_tableau_initial(tableau_initial);

    // mémorisation du tirage par l'utilisateur
    fprintf(stdout," appuyer sur Entree pour continuer");
    getchar();
    efface_ecran();
}

// encapsulation
void run(void)
{
    // définir une variable unique
    p_nombre_de_points = &nombre_de_points; 

    etape1();

    // le jeu
    questionnement(p_nombre_de_points, tableau_initial);

    // Le jeu est terminé, affichage des résultats
    affichage_resultat_final(nombre_de_points);
}



/* ---------- main ----------- */

int main(void)
{
    run();
    return EXIT_SUCCESS; 
}

/* --------------------------------- */
