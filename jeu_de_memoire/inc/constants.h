/*                                                   *
 * Auteur : Eric Bachard  version originale : 2004   *
 * Ce document est sous Licence GPL v2               *
 * voir : http://www.gnu.org/licenses/gpl-2.0.html   */

#ifndef __CONSTANTS_H
#define __CONSTANTS_H

#define VAL_MIN 30
#define VAL_MAX 250
#define ARRAY_SIZE 10

#define PREMIER "er"
#define ENIEME  "ème"

#endif /* __CONSTANTS_H */
