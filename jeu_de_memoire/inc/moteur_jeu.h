/* header moteur_de_jeu.h */

#ifndef _MOTEUR_JEU_H
#define _MOTEUR_JEU_H

short int afficher_question(int);

short int lire_reponse(void);

short affichage_resultat_question(int *, int, int, int *);

void affichage_resultat_final(int);

short int questionnement(int *, int *);

#endif /* _MOTEUR_JEU_H */
