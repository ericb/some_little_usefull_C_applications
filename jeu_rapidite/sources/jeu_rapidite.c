#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "stdbool.h"
#include "saisie.h"

short int d_coord_case = 11;
short int d_reponse_utilisateur = 46;
short int d_score = 0;
int d_duree = 0;

bool b_reponse_bonne = false;
bool b_temps_depasse = false;

time_t temps_initial = 0;
time_t temps_courant = 0;

void initialisation(void)
{
    system("clear" );
    d_score = 0;
    d_duree = 0;
    b_temps_depasse = false;
    time_t t1 = 0;
    (void)time(&t1);
    srand((long)t1);
    fprintf(stdout, "Appuyez sur la touche \"entrée\" pour lancer le jeu");
    getchar();
}

void verifier_temps(void)
{
    (void)time(&temps_courant);
    d_duree = temps_courant - temps_initial;
#ifdef DEBUG

    fprintf( stdout, "temps_initial : %ld \n", temps_initial);
    fprintf( stdout, "temps_courant : %ld \n", temps_courant);
    fprintf( stdout, "Durée : %d \n", d_duree);
#endif
    if ( d_duree > 60)
        b_temps_depasse = true;
}

void test_reponse_utilisateur(void)
{
   while ((false == b_reponse_bonne) && (false == b_temps_depasse))
   {
     if ( d_reponse_utilisateur == d_coord_case)
     {
         b_reponse_bonne = true;
         d_score += 1;
     }
     else
        fprintf( stdout, "case : %d \n", d_coord_case);
     verifier_temps();
   }
}


short int jeu(void)
{
  (void)time(&temps_initial);

  while (false == b_temps_depasse)
  {
    d_coord_case = 10*(rand()%4 +1) + (rand()%6 +1);
    fprintf(stdout, "case : %d \n", d_coord_case);
    b_reponse_bonne = true;
    fprintf( stdout, "Valeur saisie : ");
    d_reponse_utilisateur = saisie_nombre_entier_court(11, 46);
    test_reponse_utilisateur();
  }
  fprintf(stdout, "Score final : %d \n", d_score);
  return EXIT_SUCCESS;
}

int main (void)
{
    initialisation();
    jeu();
    return EXIT_SUCCESS;
}

