/*                                                   *
 * Auteur : Eric Bachard  version originale : 2002   *
 * Ce document est sous Licence GPL v2               *
 * voir : http://www.gnu.org/licenses/gpl-2.0.html   */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (void) 
{
  /* on utilise ici une structure pour manipuler plus facilement
   * les donnees			 */
  struct 
  {
    char nom[500];
    char prenom[500];
  } fiche ;

  FILE *fichier;
  /* fichier est le nom local du fichier */

/*  fichier=fopen("../ressources/data.txt","r");*/
    fichier=fopen("../ressources/adresses.txt","r");
    /*initialisation du fichier qui representera le fichier adresses.txt ouvert */

    /* Verification d'usage */
    if ( fichier == NULL) 
    { 
      fprintf(stdout, "Probleme d'ouverture de fichier"); 
      exit(-1) ;
    }

    fprintf(stdout, "Listes des noms et prenoms...\n");

    while (!feof(fichier)) 

    /* tant qu'on  n'est pas a la fin du fichier */
    {
      /* 	on recupere le nom et le prenom de l'individu et
        on stocke tout cela dans la structure fiche */
      fscanf(fichier, "%s %s", (char *)&fiche.nom, (char *)&fiche.prenom);
      /*  fscanf(fichier, "%s %s", &fiche.nom, &fiche.prenom);*/

      /* aussitot affiche a l'ecran */ 
      fprintf(stdout, "%s %s \n",  fiche.nom, fiche.prenom );
      /*
        if (fichier)
          fflush(stdout);
      */
      /* force l'ecriture à l'ecran */

    }

    fclose(fichier);
    /*on ferme le fichier) */

    return(1); /* il faut retourner quelque chose, c'est une fonction...*/
}

