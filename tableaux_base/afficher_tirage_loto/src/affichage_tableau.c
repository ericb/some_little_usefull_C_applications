/* affichage_tableau.c */

#include "affichage_tableau.h"

short int tirage_loto[ARRAY_SIZE] = { 2, 18, 43, 7, 15, 6 };

void afficher_tableau(int taille)
{
    for (short int indice = 0 ; indice < taille ; indice++)
    {
        fprintf(stdout, "Boule numéro %hd = %2hd\n", indice + 1, tirage_loto[indice]); 
    }
}


int main(void)
{
    /* TODO initialisation du tableau ICI */
    // short int tirage_loto[ARRAY_SIZE] = { 2, 18, 43, 7, 15, 6 };

    afficher_tableau(ARRAY_SIZE);

    return EXIT_SUCCESS;
}

