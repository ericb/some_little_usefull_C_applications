/* afficher_chaine.h */


#ifndef __AFFICHER_CHAINE_H
#define __AFFICHER_CHAINE_H

#include <stdio.h>
#include <stdlib.h>

#define ARRAY_SIZE  32

void afficher_chaine(int);

#endif /*  __AFFICHER_CHAINE_H */
