/* afficher_chaine.c */

#include "afficher_chaine.h"

char chaine[ARRAY_SIZE] = "Ville de Paris";
//char chaine[ARRAY_SIZE] =
//       { 'V', 'i', 'l', 'l', 'e', ' ', 'd', 'e', ' ', 'P', 'a', 'r',  'i', 's', '\0' };

void afficher_chaine(int taille_chaine)
{
    short int indice = 0;
    while ((chaine[indice] != '\0') && (indice < ARRAY_SIZE))
    {
        fprintf(stdout, "%c", chaine[indice]);
        indice++;
    }
    fprintf(stdout, "\n");
    fprintf(stdout, "Nombre de caractères :  %hd\n", indice);
}

int main(void)
{
    afficher_chaine(200);

    return EXIT_SUCCESS;
}