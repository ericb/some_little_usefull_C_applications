/* afficher_chaine.c */

#include "afficher_chaine.h"

char chaine[ARRAY_SIZE] = "Ville de Paris";
//char chaine[ARRAY_SIZE] =
//       { 'V', 'i', 'l', 'l', 'e', ' ', 'd', 'e', ' ', 'P', 'a', 'r',  'i', 's', '\0' };

void afficher_chaine(int taille_chaine)
{
    for (short int indice = 0; indice < taille_chaine ; indice++)
    {
        fprintf(stdout, "Lettre numéro %hd : %c\n", indice + 1, chaine[indice]);

        if (chaine[indice] == '\0')
            break;
    }
//    fprintf(stdout, "\n");
}

int main(void)
{
    afficher_chaine(200);

    return EXIT_SUCCESS;
}