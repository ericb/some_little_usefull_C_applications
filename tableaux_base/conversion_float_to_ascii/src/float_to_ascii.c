#include <stdio.h>
#include <stdlib.h>

#define ARRAY_SIZE 16
int main(void)
{
    const float un_nombre = 13.22f;
    char ascii[ARRAY_SIZE];

    sprintf(ascii, "%.2f", un_nombre);

    fprintf(stdout, "Avant conversion, la chaîne ascii contient :  %s\n", ascii);

    int i = 0;

    while ((ascii[i] != '\0') && (i<ARRAY_SIZE) )
    {
        do
        {
            fprintf(stdout, "ascii[%d] vaut :  %c\n",i, ascii[i]);

            if (ascii[i] == '.')
                ascii[i] = ',';

            i++;

            // très très important de s'arrêter quand la chaîne de caractères se termine
            if (ascii[i] == '\0')
                break;

        } while (i <ARRAY_SIZE);
    }

    fprintf(stdout, "Après conversion, la chaîne ascii contient :  %s\n", ascii);
    fprintf(stdout, "un_nombre vaut toujours :  %.2f\n", un_nombre);

    return EXIT_SUCCESS;
}
