/* Auteur:  Eric Bachard  / 17 novembre 2014
 * Ce document est sous Licence GPL v2
 * voir : http://www.gnu.org/licenses/gpl-2.0.html
 */

#ifndef __HASHAGE_H
#define __HASHAGE_H

#include <stdbool.h>

#define STRING_SIZE 32

/**
 * \brief an item contains a name, a value and an information providing the location in the file.
 */
typedef struct item {

  /* on stocke une chaîne de caractère */
  char key[STRING_SIZE];
  /*On suppose la valeur contenue est entiere*/
  int value;

  /* dernier élément ? (FIXME : pas encore utilisé)*/
  bool b_isLast;

  /* pointeur sur l'element suivant */
  struct item *next;
} HASH;

/**
 * \brief returns the name
 */

int getHashValue (HASH *, char [STRING_SIZE]);

/**
 * \brief init hash entry
 */


HASH * initHash (void);

/**
 * \brief deletes a key entry
 */

void deleteKey (HASH * , char [STRING_SIZE] );


/**
 * \brief removes one entry in the table
 */

/* pop */
void pop (HASH *);


/**
 * \brief adds one entry in the table
 */

/* push */
void push  (HASH *, char[STRING_SIZE], int);



/**
 * \brief nulls the table
 */

void eraseTable (void);

void display(void);

bool b_IsHashKey (HASH *, char[STRING_SIZE] );

#endif /* __HASHAGE_H */


