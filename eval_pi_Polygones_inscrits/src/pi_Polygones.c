#include <stdio.h>
#include <stdlib.h>

//#define _GNU_SOURCE


#include "saisie.h"
//#ifndef _USE_MATH_DEFINES
//#define _USE_MATH_DEFINES
//#endif
#include <math.h>

#ifndef  M_PI
#define  M_PI  3.1415926535897932384626433
#define  M_PI  3.1415926535897932384626433
#endif

#define VAL_MIN 5

#define VAL_MAX 500000

// FROM math.h : #define M_PI   3.14159265358979323846 (from math.h)

//#define DEBUG_ME

int main (void)
{
    fprintf(stdout, "Entrer le nombre de sommets à utiliser pour faire le calcul (entre %d et %d): ", VAL_MIN, VAL_MAX);

    long int nbre_sommets = saisie_nombre_entier_long(VAL_MIN, VAL_MAX);

    long double value = (long double) nbre_sommets * cos ((long double)M_PI/2.0f -((long double)M_PI/(long double)nbre_sommets));

    fprintf (stdout, "\nMéthode des polygones pour calculer Π\n\n");
    fprintf (stdout, "Dans math.h, on a    :                M_PI = 3.14159265358979323846 \n\n");
    fprintf (stdout, "En prenant un polygone de %ld sommets, le résultat vaut: %.14Lf (soit Y décimales justes sur X) \n\n",nbre_sommets, value);

    return EXIT_SUCCESS;
}


