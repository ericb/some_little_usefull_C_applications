/* pointeurs1.c  essais de swap sur le tas Eric Bachard 2024/10/25 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint-gcc.h>

int swap_valeurs(uint8_t valeur1, uint8_t valeur2)
{
    uint8_t temp = valeur2;
    fprintf(stdout, "Avant : v1 = %d   v2 = %d  \n", valeur1, valeur2);
    valeur2 = valeur1;
    valeur1 = temp;

    fprintf(stdout, "Après : v1 = %d   v2 = %d  \n", valeur1, valeur2);
    return EXIT_SUCCESS;
}


int main(void)
{
    uint8_t valeur_a = 65;
    uint8_t valeur_b = 66;

    fprintf(stdout, "Avant : valeur_a = %d   valeur_b = %d  \n", valeur_a, valeur_b);

    short int anErr = swap_valeurs(valeur_a, valeur_b);

    if (anErr != 0)
        fprintf(stderr, "Pb pendant le swap des valeurs");

    fprintf(stdout, "Après : valeur_a = %d   valeur_b = %d  \n", valeur_a, valeur_b);

    return EXIT_SUCCESS;
}

