#include <stdio.h>
#include <stdlib.h>

#include <math.h>

#define NBR_ITERATIONS 2000000

int main (void)
{
    long double sum = 0.0f;
    int k = 1;

    for (int i = 0 ; i < NBR_ITERATIONS; i++)
    {
        long double next = k /(2*i+1.0f);
        sum += next;

#ifdef DEBUG_ME
        fprintf (stdout, "Après %d itérations, -1^k/(2k + 1) = %Lf, k = %d et le résultat vaut: %Lf\n ", i, k/(2*i+1.0f), k, 4.0f*sum);
#endif

        k = - k;
    }

    fprintf (stdout, "Après %d itérations, le résultat vaut: %.14Lf\n ",NBR_ITERATIONS, 4.0f*sum);

    return EXIT_SUCCESS;
}