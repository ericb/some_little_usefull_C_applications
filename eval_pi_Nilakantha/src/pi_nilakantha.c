#include <stdio.h>
#include <stdlib.h>

#include <math.h>

const long int  NBR_ITERATIONS = 20000000;

// FROM math.h : #define M_PI   3.14159265358979323846 (from math.h)

//#define DEBUG_ME

int main (void)
{
    long double sum = 0.0f;

    // optimisation : manipuler le plus d'entiers possibles
    // éviter les divisions et les multiplications. Préférer les addtions

    long int k = 1;
    long int l = 2;
    long int m = 3;
    long int n = 4;

    for (int i = 0 ; i < NBR_ITERATIONS; i++)
    {
        // produit simple
        long double denum = l * m * n;
        long double num = 4.0f * k;

        // simple addition (de double, c'est + délicat)
        sum += num / denum;
#ifdef DEBUG_ME
        fprintf (stdout, "Après %ld itérations, k = %ld et le résultat vaut : %Lf\n", i, k, 3.0f +sum);
#endif
        // multiplication par -1 est une opération simple
        k = - k;
        l += 2;
        m += 2;
        n += 2;
    }

    fprintf (stdout, "\nMéthode de Nilakantha pour calculer Π\n\n");
    fprintf (stdout, "Dans math.h, on a    :                M_PI = 3.14159265358979323846 \n\n");
    fprintf (stdout, "Après %ld itérations, le résultat vaut: %.14Lf (soit 11 décimales justes sur 14) \n\n",NBR_ITERATIONS, 3.0f + sum);

    return EXIT_SUCCESS;
}