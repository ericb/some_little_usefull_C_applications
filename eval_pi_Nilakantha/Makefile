# Makefile pour le projet efface_ecran
# incluant une nouvelle façon de créer un binaire exécutable
# + une nouvelle façon de sécuriser ce binaire.

# Auteur : Eric Bachard
# Ce document est sous Licence GPL v2  23 Septembre 2024
# voir : http://www.gnu.org/licenses/gpl-2.0.html

UNAME_S = `uname -s`
BUILD_DIR = build
APPLICATION_NAME = pi_Nilakantha
BINARY_NAME = ${BUILD_DIR}/${APPLICATION_NAME}

# please adapt the value to the gcc version
# available on your machine
#GCC_VERSION = -10

CC = gcc${GCC_VERSION}

INCLUDE_DIR = inc
SOURCES_DIR = src
DEBUG_EXT = _debug
C_STD=-std=c99

CFLAGS = -I${INCLUDE_DIR} -Wall -ansi -pedantic ${C_STD}
CFLAGS_DEBUG = -g -DDEBUG

include secu.mk

SOURCES = \
        ${SOURCES_DIR}/pi_nilakantha.c \

# works but looks buggy (only the first item of the list contains the right prefix)
# that's the reason why the rule is slightly modified in the final collect
OBJS = $(addsuffix .o, $(basename $(notdir $(SOURCES))))
D_OBJS = $(addsuffix .do, $(basename $(notdir $(SOURCES))))

##---------------------------------------------------------------------
## BUILD RULES
##---------------------------------------------------------------------

%.o:${SOURCES_DIR}/%.c
	$(CC) ${C_STD} ${EXTRA_FLAGS} ${CFLAGS} -c -o ${BUILD_DIR}/$@ $<

%.do:${SOURCES_DIR}/%.c
	$(CC) ${C_STD} ${EXTRA_FLAGS} ${CFLAGS} ${CFLAGS_DEBUG} -c -o ${BUILD_DIR}/$@ $<

all: ${BINARY_NAME} ${BINARY_NAME}${DEBUG_EXT}
	@echo Build complete for ${UNAME_S}

${BINARY_NAME}: ${OBJS}
	$(CC) $(C_STD) ${EXTRA_FLAGS} -o $@ ${BUILD_DIR}/*.o ${CFLAGS} $(LIBS)
	rm -f ${BUILD_DIR}/*.o

${BINARY_NAME}_debug: ${D_OBJS}
	$(CC) $(C_STD) ${EXTRA_FLAGS} -o $@ ${BUILD_DIR}/*.do ${CFLAGS} ${CFLAGS_DEBUG} $(LIBS)
	rm -f ${BUILD_DIR}/*.do

clean:
	@echo  Effacement de : ${BINARY_NAME}*
	rm -f ${BINARY_NAME}* ${BUILD_DIR}/*.o ${BUILD_DIR}/*.do


