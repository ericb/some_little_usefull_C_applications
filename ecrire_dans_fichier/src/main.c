/* main.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ecrire_dans_fichier.h"
#include "replace_dot.h"

int main(void)
{
    float point_x = 0.123f;
    float point_y = 18.184f;

    char ascii_x[ARRAY_SIZE];
    char ascii_y[ARRAY_SIZE];

    sprintf(ascii_x, "%f", point_x);
    sprintf(ascii_y, "%f", point_y);

    replace_dot(ascii_x);
    replace_dot(ascii_y);

    strcat(ascii_x, ";");
    strcat(ascii_x, ascii_y);

    if (create_header() != EXIT_SUCCESS)
    {
        fprintf(stderr, "Pb avec la création d'en-tête");
        return EXIT_FAILURE;
    }

    if (ecrire_dans_fichier(ascii_x) != EXIT_SUCCESS)
    {
        fprintf(stderr, "Pb en écrivant dans fichier");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
