/* replace_dot.c */

#include <stdio.h>
#include "ecrire_dans_fichier.h"

void replace_dot(char * const ascii)
{
    int i = 0;

    while ((ascii[i] != '\0') && (i<ARRAY_SIZE) )
    {
        do
        {
#ifdef DEBUG
            fprintf(stdout, "ascii[%d] vaut :  %c\n",i, ascii[i]);
#endif
            if (ascii[i] == '.')
                ascii[i] = ',';

            // très très important de s'arrêter quand la chaîne de caractères se termine !!
            if ('\0' == ascii[i])
                break;

            i++;

        } while (i < ARRAY_SIZE);
    }
}
