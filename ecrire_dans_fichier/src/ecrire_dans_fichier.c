/* ecrire dans fichier.c   2024/12/09 Eric Bachard */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ecrire_dans_fichier.h"

int create_header(void)
{
    if (ecrire_dans_fichier("\"Coordonnée X\";\"Coordonnée Y\"") != EXIT_SUCCESS)
        return EXIT_FAILURE;
    else
        return EXIT_SUCCESS;
}


int ecrire_dans_fichier(const char* aString)
{
    FILE* fichier = NULL;

#ifdef DEBUG
    fprintf(stderr, "Fonction en cours d'exécution : %s \n", __func__ );
    fprintf(stdout, "Nom du fichier ouvert en lecture / écriture :  %s \n", (char *)fichier );
#endif
    fichier = fopen(CSV_FILENAME, "a");

    if (fichier != NULL)
    {
        fprintf(fichier, "%s\n", aString);

        if (fclose(fichier) != 0)
        {
            fprintf(stderr, "Probleme de fermeture du fichier\n");
            return (EXIT_FAILURE);
        }
    }
    else
    {
        fprintf(stderr, "Problème d'ouverture de fichier !\n");
        return (EXIT_FAILURE);
    }

    return EXIT_SUCCESS;
}
