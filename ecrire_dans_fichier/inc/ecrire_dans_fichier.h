/* ecrire_dans_fichier.h */

#ifndef __ECRIRE_DANS_FICHIER_H
#define __ECRIRE_DANS_FICHIER_H

#define ARRAY_SIZE 1024

#define CSV_FILENAME "../ressources/simulation.csv"

int create_header(void);

int ecrire_dans_fichier(const char *);


#endif /* __ECRIRE_DANS_FICHIER_H */
