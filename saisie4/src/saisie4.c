/* saisie4.c  Bachard Eric  2024_11_07 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "saisie.h"

int main(void)
{
    fprintf(stdout, "Saisir la composante XA comprise entre 0.0 et 1024 :  " );
    float X_A = saisie_nombre_reel(0,1024);

    fprintf(stdout, "Saisir la composante YA comprise entre 0.0 et 768 :  " );
    float Y_A = saisie_nombre_reel(0,1024);

    fprintf(stdout, "Saisir la composante XB comprise entre 0.0 et 1024 :  " );
    float X_B = saisie_nombre_reel(0,1024);

    fprintf(stdout, "Saisir la composante YB comprise entre 0.0 et 768 :  " );
    float Y_B = saisie_nombre_reel(0,768);


    float distance = sqrtf((X_A - X_B)* (X_A -X_B) + (Y_A - Y_B)* (Y_A -Y_B));
    fprintf(stdout, "La distance entre les 2 points vaut : %3.3f \n", distance);

    return EXIT_SUCCESS;
}