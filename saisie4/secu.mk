# add security compilation flags there

GCC_SECURITY_FLAGS = -fstack-protector \
                     -Wwrite-strings \
                     -fno-builtin-memset \
                     -pie \
                     -fPIE \
                     -D_FORTIFY_SOURCE=2 \
                     -Wformat \
                     -Wformat-security

EXTRA_FLAGS += ${GCC_SECURITY_FLAGS}

