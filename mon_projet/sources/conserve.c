#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

int main(void)
{
    float Volume = 0.0;
    fprintf(stdout, "volume ? : ");
    fscanf(stdin, "%f,\n", &Volume);

    float R =  powl ( ( (0.001 * Volume) / (2 * M_PI) ) , 1.0/3.0);
    float H = Volume/M_PI * R*R;

   fprintf(stdout, "la hauteur vaut: %f , le rayon vaut: %f\n", H, R);

   return EXIT_SUCCESS;
}
