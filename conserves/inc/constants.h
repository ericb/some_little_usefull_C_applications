/* constants.h */
/* programme de calcul des dimensions d'une boite de conserve circulaire
 * dans le but de minimiser la surface de métal utilisée.
 * Auteur:  Eric Bachard  / 3 novembre 2014
 * Ce document est sous Licence GPL v2
 * voir : http://www.gnu.org/licenses/gpl-2.0.html
 */

#ifndef __CONSTANTS_H
#define __CONSTANTS_H

#define VALEUR_MIN 0
#define VALEUR_MAX 20
#define TAILLE_BUFFER 20

#endif /* __CONSTANTS_H */
