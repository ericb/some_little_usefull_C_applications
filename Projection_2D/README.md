Pour compiler le programme, cette ligne de commande devrait fonctionner :


gcc -Wall -Wextra -o project  projection.c  -lm 


Toutefois, il est recommandé d'essayer de le compiler sans le "-lm" final pour comprendre que
les fonctions sin() et cos() sont implémentées (le binaire qui exécute le code) dans la libm.so.