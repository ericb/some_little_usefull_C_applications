#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define M_PI 3.14159265358979323846

float angle1 = 10.0f;
float distance = 1.0f;


int main(void)
{
    float A_x = distance * cos(angle1 * M_PI /180.0);
    float A_y = distance * sin(angle1 * M_PI /180.0);

    fprintf( stdout, "Composante x =  %3.3f , composante y = %3.3f \n" , A_x , A_y  );

    return EXIT_SUCCESS;
}
