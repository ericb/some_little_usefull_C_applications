# COURS

JEU : pour celles et ceux qui n'ont pas remarqué, des documents de cours sont proposés quelque part dans ce dépôt

Indications : find . -t d -name Partie1

# APPLICATIONS

Quelques applications en C très utiles, **classées par difficulté croissante**, et telles que :

## Niveau 1
- [x] Utilitaires : quelques astuces sous Linux, avec le shell bash
- [X] Compilation d'un binaire : mise en évidence des étapes de la compilation d'un binaire + introduction à gdb
- [x] efface_ecran : effacement de l'écran avec popen (au lieu de "system", qui est bloquant )
- [x] portée variables: TP sur la visibilité des variables (prépare le même concept en C++)
- [x] bIsEven : petit programme qui permet de savoir si un nombre est pair ou impair (tirage de ~ 10 nombres)
- [x] conserves : petit calcul d'optimisation, basé sur l'utilisation de la dérivée
- [x] combien_nombre_pairs : algorithme basique de comptage de nombres pairs entre 2 bornes
- [x] tableaux_base : initiation à l'utilisation de tableaux en C (taille fixe)
- [ ] structures : 3 petits programmes permettant d'illustrer la notion de structure
- [x] saisie : API d'entrées sorties ultra simple, mais robuste (merci de faire remonter les problèmes)
- [x] convert float to ascii : introduit avec les tableaux et surtout pour montrer à quel point il est dangereux de ne pas terminer les chaînes de caractères.
## Niveau 2
- [ ] Écrire dans fichier : permet de créer un fichier texte et d'écrire dedans. D'autres méthodes intéressantes ont été ajoutées.
- [ ] Algorithme de Jules César personnalisé : permet de chiffrer / déchiffrer une chaine de caractères
- [ ] utime : gestion du temps utilisant CLOCK_REALTIME, gettimeofday() et timespec + génération d'un nombre aléatoire
- [ ] eval_pi_Polygones_inscrits : permet d'évaluer Π avec la méthode dite des polygones
- [ ] eval_pi_Monte_Carlo : permet d'évaluer Π avec la méthode de Monte Carlo (peu précise et lente)
- [ ] eval_pi_Leibnitz : permet d'évaluer Π avec la méthode de Leibnitz (rapide)
- [ ] eval_pi_Nilakantha : permet d'évaluer Π avec la méthode de Nilakantha (la plus performante)

## Niveau 3
- [ ] mot_de_passe : saisie de caractères sans "echo" (Linux seulement). Permet de saisir un mot de passe sans écho et de l'afficher ensuite
- [ ] argv : permet d'exploiter la saisie d'arguments ajoutés dans la ligne de commande. (pointeurs)
- [ ] lecture_fichier_statique : programme qui permet d'afficher le contenu d'un fichier texte (nom défini en dur dans le programme)
- [ ] lecture dynamique : programme qui permet d'afficher le contenu d'un fichier texte

## Niveau 4
- [ ] jeu de mémoire : jeu qui permet à un utilisateur de tester sa mémoire
- [ ] fonction_table_de_hashage : exemple d'implémentation naïve d'une table de hashage
- [ ] jeu rapidité : saisir le plus rapidement possible le numéro de case proposé aléatoirement

## Niveau 5
Ci-dessous quelques exemples de projets réalisés par des élèves (langage : C)
- [ ] Learn_IT : création d'un dictionnaire avec l'utilisation de strtok
- [ ] OSS Sans Baguette : Jeu MMRPG sous la forme d'une base de données (principe d'une base de données)
- [ ] QR Projet Linux : les bases du QR Code (ne marche pas)

