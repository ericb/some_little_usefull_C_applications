/* affichage.c  Eric Bachard 2024_11_12 */

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    short int age = 18;
    long int argent = 18000000;
    float temperature = 19.26;
    double base_log_neperien = 2.711828;
    long double my_pi = 3.141592654;

    fprintf(stdout, "age =%hd \n", age);
    fprintf(stdout, "argent = %ld \n",argent );
    fprintf(stdout, "temperature = %f \n", temperature);
    fprintf(stdout, "bses_log_neperien = %f \n",base_log_neperien );
    fprintf(stdout, "my_pi = %Lf \n", my_pi);

    return EXIT_SUCCESS;
}

